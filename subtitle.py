import time
import re
from googletrans import Translator
tl = Translator()

delay = 0
enc_in = "utf-8"
enc_out = "utf-8"

#Set file name and open
valid=False
while valid == False:
    #File Name entry
    tf=input("File Name: ")
    #Check entry
    if tf[len(tf)-4:] != ".srt":
        print("Not an .srt file")
    else:
        try:
            f = open(tf)
            print("File accepted")
            valid=True
        except IOError:
            print("Not a valid file name / file not in this directory")

LANG_SHORT = ['af', 'sq', 'am', 'ar', 'hy', 'az', 'eu', 'be', 'bn', 'bs', 'bg', 'ca', 'ceb',
              'ny', 'zh-cn', 'zh-tw', 'co', 'hr', 'cs', 'da', 'nl', 'en', 'eo', 'et', 'tl',
              'fi', 'fr', 'fy', 'gl', 'ka', 'de', 'el', 'gu', 'ht', 'ha', 'haw', 'iw', 'he',
              'hi', 'hmn', 'hu', 'is', 'ig', 'id', 'ga', 'it', 'ja', 'jw', 'kn', 'kk', 'km',
              'ko', 'ku', 'ky', 'lo', 'la', 'lv', 'lt', 'lb', 'mk', 'mg', 'ms', 'ml', 'mt',
              'mi', 'mr', 'mn', 'my', 'ne', 'no', 'or', 'ps', 'fa', 'pl', 'pt', 'pa', 'ro',
              'ru', 'sm', 'gd', 'sr', 'st', 'sn', 'sd', 'si', 'sk', 'sl', 'so', 'es', 'su',
              'sw', 'sv', 'tg', 'ta', 'te', 'th', 'tr', 'uk', 'ur', 'ug', 'uz', 'vi', 'cy',
              'xh', 'yi', 'yo', 'zu']
LANG_LONG = ['afrikaans', 'albanian', 'amharic', 'arabic', 'armenian', 'azerbaijani', 'basque',
             'belarusian', 'bengali', 'bosnian', 'bulgarian', 'catalan', 'cebuano', 'chichewa',
             'chinese (simplified)', 'chinese (traditional)', 'corsican', 'croatian', 'czech',
             'danish', 'dutch', 'english', 'esperanto', 'estonian', 'filipino', 'finnish',
             'french', 'frisian', 'galician', 'georgian', 'german', 'greek', 'gujarati',
             'haitian creole', 'hausa', 'hawaiian', 'hebrew', 'hebrew', 'hindi', 'hmong',
             'hungarian', 'icelandic', 'igbo', 'indonesian', 'irish', 'italian', 'japanese',
             'javanese', 'kannada', 'kazakh', 'khmer', 'korean', 'kurdish (kurmanji)', 'kyrgyz',
             'lao', 'latin', 'latvian', 'lithuanian', 'luxembourgish', 'macedonian', 'malagasy',
             'malay', 'malayalam', 'maltese', 'maori', 'marathi', 'mongolian',
             'myanmar (burmese)', 'nepali', 'norwegian', 'odia', 'pashto', 'persian', 'polish',
             'portuguese', 'punjabi', 'romanian', 'russian', 'samoan', 'scots gaelic',
             'serbian', 'sesotho', 'shona', 'sindhi', 'sinhala', 'slovak', 'slovenian',
             'somali', 'spanish', 'sundanese', 'swahili', 'swedish', 'tajik', 'tamil', 'telugu',
             'thai', 'turkish', 'ukrainian', 'urdu', 'uyghur', 'uzbek', 'vietnamese', 'welsh',
             'xhosa', 'yiddish', 'yoruba', 'zulu']

#Get source language
valid=False
while valid == False:
    #Test flag entry
    lr=input("Source/Input language: ")
    inlang=lr.lower()
    if inlang in LANG_SHORT:
        langin = inlang
        print("Translating from: " + langin + ", " + LANG_LONG[LANG_SHORT.index(langin)])
        valid=True
    elif inlang in LANG_LONG:
        langin = LANG_SHORT[LANG_LONG.index(inlang)]
        print("Translating from: " + langin + ", " + inlang)
        valid=True
    else:
        print("Not a valid Language, check the list")

#Get output language
valid=False
while valid == False:
    #Test flag entry
    lr=input("Destination/Output language: ")
    outlang=lr.lower()
    if outlang in LANG_SHORT:
        langout = outlang
        print("Translating to: " + langout + ", " + LANG_LONG[LANG_SHORT.index(langout)])
        valid=True
    elif outlang in LANG_LONG:
        langout = LANG_SHORT[LANG_LONG.index(inlang)]
        print("Translating to: " + langout + ", " + outlang)
        valid=True
    else:
        print("Not a valid Language, check the list")


output = []
appasis = False

#List building
with open(tf, encoding=enc_in) as f: 
    for line in f:
        if line == "\n":  # Blank lines will be appended as-is
            appasis = True
        else:
            try:
                int(line)  # Lines which are just numbers are appended as-is
                appasis = True
            except ValueError:  # Lines which are time codes are appended as-is
                if re.match('[0-9][0-9]:[0-5][0-9]:[0-5][0-9],[0-9][0-9][0-9] -->', line):
                    appasis = True
        if appasis:
            output.append(line)
        else:  # Line is text to be translated
            newtext = tl.translate(line, src=langin, dest=langout)
            output.append(newtext.text + "\n")
        time.sleep(delay)
        appasis = False
f.close()

#Write to new file
newfile= tf[:-4] + "_" + outlang + tf[-4:]
f=open(newfile,"w+",encoding=enc_out)
for line in output:
    if "\u200b" in line:
        line = line.replace("\u200b","")
    try:
        f.write(line)
    except UnicodeEncodeError:
        print("Line encoding error: " + line)
        print("try running file save block again")
f.close()

print('Successfully saved to file: "' + newfile + '"')
