**Sub.Trans - Subtitle Translation Tool**

**Summary**\
This Python3 script allows you to automatically translate .SRT subtitles files with Google Translate. 

**Why**\
I've been learning to speak Russian and have enjoyed watching some Russian television shows. I eventually came across a show called Чернобыль - Зона отчуждения (*Chernobyl - Exclusion Zone*).
A fictional drama about time travel and attempting to prevent the Chernobyl nuclear disaster. Unfortunately for me, I could not find any source for English subtitles. I could find a set of
Bulgarian subtitles though, and a Python library called googletrans that lets you easily pipe text through the Google Translate API. Combining the two I got pretty decent English subtitles,
good enough to watch the show and understand what's going on at least.

**How**\
The script works like many of my others, with the interactive shell. You run sub.skew.py in the directory with your subtitle file(s). You are prompted to supply the following:\
File name (any .SRT file),\
Source language (language of the existing subtitles),\
Destination language (desired language of the subtitles),\
Once you have entered all the required information the script processes each line (skipping newlines and timecodes). At the end it creates a new .SRT file in the same directory with the same 
name plus an underscore and the abbreviated destination language.

**Tips**\
You will need to install the googletrans module for Python before using this script.

You will need to check the encoding format for the files you are using. The default setting in the script is UTF-8 but if your SRT file(s) are not UTF-8 formatted this will cause issues.
If this is the case you need to either change the variable at the beginning of the script to match your file(s) formatting, or convert your file(s) to UTF-8 before translating.
The variable for the output file is in the same place if your destination language contains characters that are not compatible with UTF-8.

The language selection accepts both full words and abbreviations that are accepted by the Google Translate API. Check the script itself to see all of the potential language options.

I had at one point run into an error 429 rate limiting issue during early development, so I added the ability to limit the speed of the requests by adding a delay. When I revisted
this issue while refining the script I could not replicate the error. I had found a potential solution in a post online, creating a new instance of the Translator() when an
exception gets thrown, I couldn't get the bug to happen again though so I never implemented it. I have left the delay at 0 for these reasons but if this error affects you the variable 
is at the top of the script. Just increase the value slightly, 1 second is likely more than enough. Or you could try coding in the fix I mentioned.